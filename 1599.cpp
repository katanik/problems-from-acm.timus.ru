#include<iostream>
#include<vector>
#include<algorithm>
#define f first
#define s second
#define P pair<pt, pt>
using namespace std;
typedef double db;

struct pt {
	db x, y;
	pt () {}
	pt (db x, db y) : x(x), y(y) {}
	pt operator- (pt p) {return pt(x-p.x, y-p.y);}
	pt operator+ (pt p) {return pt(x+p.x, y+p.y);}
	pt operator* (db p) {return pt(x*p, y*p);}
	db operator^ (pt p) {return x*p.y-y*p.x;}
	void operator-= (pt p) {*this=(*this)-p;}
	void operator+= (pt p) {*this=(*this)+p;}
	void operator*= (db p) {*this=(*this)*p;}
	bool operator== (pt p) {return x==p.x && y==p.y;}
	bool check (pt a, pt b) {return min(a.x, b.x)<=this->x && this->x<=max(a.x, b.x) && min(a.y, b.y)<=this->y && this->y<=max(a.y, b.y);}
	bool lie (pt a, pt b) {return ((*this-a)^(*this-b))==0 && (*this).check(a, b);}
	void in () {scanf("%lf%lf", &x, &y);}
};

vector <P> v;
int kol1=0, kol2=0;

pt rey(-1, 0);

bool find_cross (pt & a, pt & b, pt & p, pt & cur) {
	pt v=b-a;
	if (((b-a)^rey)==0) 
		return 0;
	db t=((p-a)^rey)/((b-a)^rey);
	cur=a+(b-a)*t;
	return 1;
}

int binsearch (pt & p){
	int l=0, r=v.size()-1;
	while (l<r){
		int m=(l+r)/2;
		if (max(v[m].s.y, v[m].f.y)<p.y)
			l=m+1;
		else
			r=m;			
	}
	return l;
}

void solve (pt & p){
	pt p1=p, cross;
	int i=binsearch (p), kol=0; i=i==0?0:i;
	for (; i<v.size(); i++){
		if (max(v[i].s.y, v[i].f.y)<p.y || min(v[i].s.y, v[i].f.y)>p.y) continue;
		if (p.lie(v[i].f, v[i].s)) {printf("EDGE\n"); return;}
		if (!find_cross(v[i].f, v[i].s, p, cross)) continue;
		if (cross.x>p.x) continue;
		if (((v[i].f-p)^rey)==0 || ((v[i].s-p)^rey)==0) {p.y+=0.5; find_cross(v[i].f, v[i].s, p, cross);}
		if (cross.check(v[i].f, v[i].s)){
			if (((v[i].f-v[i].s)^rey)<0) kol--;
			else kol++;
		}
		p=p1;
	}		
	printf("%d\n", kol);
}

bool cmp (P & a, P & b) { 
	return max(a.first.y, a.second.y)<max(b.first.y, b.second.y);
}

int main() {
	int n, m;
	scanf("%d", &n);
	vector <pt> a(n);
	for (int i=0; i<n; i++)	a[i].in();
	a.push_back(a[0]); 
	for (int i=0; i<n; i++) v.push_back(P(a[i], a[i+1]));
	sort(v.begin(), v.end(), cmp);
	scanf("%d", &m);
	pt p; 
	for (int i=0; i<m; i++){
		p.in(); solve(p);
	}
}