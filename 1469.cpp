#include<iostream>
#include<vector>
#include<map>
#include<iterator>
#include<functional>
#include<algorithm>

using namespace std;

typedef pair<int, int> P;
typedef long long ll;
typedef double db;

const db eps = 1e-9;
db X, Y;

struct Point {
	int x, y;
	Point(int x = 0, int y = 0) : x(x), y(y) {}
	~Point() {}
	Point operator- (Point p) { return Point(x - p.x, y - p.y); }
	int operator^ (Point p) { return x * p.y - y * p.x; }
	void in() { scanf_s("%d%d", &x, &y); }
};

struct Segment {
	Point a, b;
	Segment() {}
	Segment(Point a, Point b) : a(a), b(b) {}

	~Segment() {}

	db y() {
		if (b.x == a.x) return (Y >= min(b.y, a.y) && Y <= max(b.y, a.y)) ? Y : a.y;
		return (db)a.y + (db)(b.y - a.y)*(db)(X - a.x) / (db)(b.x - a.x);
	}
	bool operator< (Segment s) { return (*this).y() < s.y(); }
	bool operator<= (Segment s) { return (*this).y() <= s.y(); }
	bool operator== (Segment s) {
		return fabs((*this).y() - s.y()) < eps;
	}
	bool operator^ (Segment s) {
		bool f = ((s.b - a) ^ (s.a - s.b)) * 1ll * ((s.b - b) ^ (s.a - s.b)) <= 0 && ((b - s.a) ^ (a - b)) * 1ll * ((b - s.b) ^ (a - b)) <= 0;
		return check(a.x, b.x, s.a.x, s.b.x) && check(a.y, b.y, s.a.y, s.b.y) && f;
	}
	void in()
	{
		a.in(); b.in();
		if (a.x > b.x)
			swap(a, b);
	}

private:
	bool check(int a, int b, int c, int d)
	{
		if (a > b) swap(a, b); if (c > d) swap(c, d);
		if (a > c) { swap(a, c); swap(b, d); }
		return c >= a && c <= b;
	}
};

struct Event
{
	int p;
	int tp, id;
	Event() {}
	Event(int p, int tp, int id) : p(p), tp(tp), id(id) {}
	bool operator< (Event e) { return p == e.p ? tp > e.tp : p < e.p; }
};


vector <Segment> segments;
vector <Event> events;

namespace Cartesian_tree
{
	class Treap
	{
		int x, y;
		Segment first, last;
		Treap * l, *r;
	public:
		Treap(int x = 0, int y = 0);
		~Treap();
		friend void merge(Treap*&, Treap*, Treap*);
		friend void split(int, Treap*, Treap*&, Treap*&);
		friend void erase(int x, Treap*&);
		int get_left();
		int get_right();
		void out(int l, int r) const;
	};

	Treap::Treap(int x, int y) : x(x), y(y), l(0), r(0)
	{
	}

	Treap::~Treap()
	{
	}

	void merge(Treap* & T, Treap * L, Treap * R)
	{
		if (!L)
		{
			T = R;
			return;
		}
		if (!R)
		{
			T = L;
			return;
		}
		if (L->y <= R->y)
		{
			merge(R->l, L, R->l);
			T = R;
		}
		else {
			merge(L->r, L->r, R);
			T = L;
		}
	}

	void split(int x, Treap * T, Treap * & L, Treap * & R)
	{
		if (!T)
		{
			L = nullptr;
			R = nullptr;
			return;
		}
		if (segments[T->x] <= segments[x])
		{
			split(x, T->r, T->r, R);
			L = T;
		}
		else
		{
			split(x, T->l, L, T->l);
			R = T;
		}
	}

	int Treap::get_left()
	{
		if (!this) return -1;
		Treap * cur = this;
		for (; cur->r; cur = cur->r);
		return cur->x;
	}

	int Treap::get_right()
	{
		if (!this) return -1;
		Treap * cur = this;
		for (; cur->l; cur = cur->l);
		return cur->x;
	}

	void Treap::out(int l, int r) const
	{
		printf("YES\n%d %d", min(l + 1, r + 1), max(l + 1, r + 1));
		exit(0);
	}

	void insert(int x, Treap*& T) {
		Treap * t, *L, *R;
		t = new Treap(x, rand());
		split(x, T, L, R);
		int l = L->get_left(), r = R->get_right();
		if (l != -1 && l != x && segments[l] ^ segments[x]) { T->out(l, x); }
		if (r != -1 && r != x && segments[r] ^ segments[x]) { T->out(r, x); }
		merge(L, L, t); merge(T, L, R);
	}

	void erase(int x, Treap * & T) {
		if (!T) return;
		if (segments[x] == segments[T->x]) { merge(T, T->l, T->r); }
		else erase(x, segments[x] <= segments[T->x] ? T->l : T->r);
	}

};

void solve() {
	using namespace Cartesian_tree;
	Treap * T = new Treap(); T = nullptr;
	sort(events.begin(), events.end());

	for (int i = 0; i < events.size(); i++) {
		int x = events[i].id;
		X = events[i].p; Y = events[i].tp == 1 ? segments[x].a.y : segments[x].b.y;
		if (events[i].tp == 1) insert(x, T);
		else {
			erase(x, T);
			Treap * L, *R;
			split(x, T, L, R);
			int l = L->get_left(), r = R->get_right();
			if (l != -1 && r != -1 && l != x && r != x && segments[l] ^ segments[r]) { T->out(l, r); }
			merge(T, L, R);
		}
	}
	printf("NO");
}

int main() {
	int n;
	scanf_s("%d", &n);
	segments.resize(n);
	for (int i = 0; i < n; i++)
	{
		segments[i].in();
		events.push_back(Event(segments[i].a.x, 1, i));
		events.push_back(Event(segments[i].b.x, -1, i));
	}
	solve();
}
