#include<iostream>
#include<vector> 
#include<string>
#include<sstream>
using namespace std;

typedef double db;

struct pt {
	db x, y, z;
	pt () {}
	pt (db x1, db y1, db z1) {x=x1, y=y1, z=z1;}	
	pt operator- (pt p) {return pt (x-p.x, y-p.y, z-p.z);}
	pt operator+ (pt p) {return pt (x+p.x, y+p.y, z+p.z);}
	pt operator^ (pt p) {return pt (y*p.z-z*p.y, -x*p.z+z*p.x, x*p.y-y*p.x);}
	pt operator* (db k) {return pt (x*k, y*k, z*k);}
	pt operator/ (db k) {return pt (x/k, y/k, z/k);}
	db operator* (pt p) {return x*p.x+y*p.y+z*p.z;}
	bool operator== (pt p) {return x==p.x && y==p.y && z==p.z;}
	void operator= (pt p) {x=p.x, y=p.y, z=p.z;}
	db mod () {return sqrt(x*x+y*y+z*z);}
	void in () {scanf("%lf%lf%lf", &x, &y, &z);}
	void out () {printf("%0.10lf %0.10lf %0.10lf\n", x, y, z);}
};

const db eps=1e-5;

pt cross (pt p1, pt v1, pt p2, pt v2) {
	pt p=(v2^v1)^v1;
	db t=-((p2-p1)*p)/(v2*p);
	return p2+v2*t;
}

pt find_p (pt p, pt a) {
	db t=-(a*p)/(a*a);
	return p+a*(-(a*p)/(a*a));
}

int main() {
	pt a, b, c, d, p1, p2;
	a.in(); b.in(); c.in(); d.in();

	if (((d-c)^(b-a)).mod()<eps){
		if (((a-c)^(b-c))==pt(0,0,0)) {a.out(); b.out(); printf("0"); return 0;}
		if (((a+c)*0.5)==(b+d)*0.5) {p1=(a+d)*0.5; p2=(b+c)*0.5;}
		else {p1=(a+c)*0.5; p2=(b+d)*0.5;}
		p1.out(); p2.out(); printf("180");
	}
	else{
		pt v1=(b-a)/(b-a).mod(), v2=(d-c)/(d-c).mod();
		pt v=v1+v2; 
		p1=(cross(a, b-a, c, d-c)+cross(c, d-c, a, b-a))/2;
		p1=find_p(p1, v);
		p2=p1+v/v.mod();
		p1.out(); p2.out(); printf("180"); 
	}
}