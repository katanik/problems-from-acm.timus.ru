#pragma comment (linker, "/STACK:67108864")
#include<vector>
#include<iostream>
#include<fstream>
#include<string>
#include<algorithm>

using namespace std;
typedef double db;

const db eps = 1e-9, pi=acos(-1.);

struct pt{
	db x, y;
	pt(){}
	pt(db x, db y) : x(x), y(y) {}
	pt operator+ (pt p){ return pt(x + p.x, y + p.y); }
	pt operator- (pt p){ return pt(x - p.x, y - p.y); }
	pt operator* (db p){ return pt(x * p, y * p); }
	db operator* (pt p) { return x*p.x + y*p.y; }
	pt turn(db ang, pt O) { return pt(O.x + (x - O.x)*cos(ang) + (y - O.y)*sin(ang), O.y - (x - O.x)*sin(ang) + (y - O.y)*cos(ang)); }
	db mod(){ return sqrt(x*x + y*y); }
};

db r1, r2, r3, r4;
pt a, b, c;

db f(db ang, pt & p1){
	pt p = (pt(b.x-r4, b.y)).turn(ang, b);
	return (p1-p).mod()+(p-a).mod();
}

db ternar2(db ang){
	pt p = (pt(c.x+r4, c.y)).turn(ang, c);
	db l = 0., r = acos(((a-b)*(p-b))/(a-b).mod()/(p-b).mod()), m1, m2;
	while (r-l>eps){
		m1 = l + (r - l) / 3.0;
		m2 = r - (r - l) / 3.0;
		if (f(m1, p) < f(m2, p)) r = m2;
		else l = m1;
	}
	return (a - p).mod() + f((r + l) / 2.0, p);
}

db ternar1(){
	db l = 0, r = pi, m1, m2;
	while ((r-l)>eps){
		m1 = l + (r - l) / 3.0;
		m2 = r - (r - l) / 3.0;
		if (ternar2(m1) < ternar2(m2)) r = m2;
		else l = m1;
	}
	return ternar2((l+r)*0.5);
}

db find_x(){return (r1*r1 + r2*r2 - r3*r3) / (2.*r1);}

db find_h(db x){ return sqrt(r2*r2 - x*x); }

int main(){
	scanf_s("%lf%lf%lf%lf", &r1, &r2, &r3, &r4);
	a = pt(0, 0);
	db x = find_x(), h = find_h(x);
	b = pt(r1, 0); c=pt(x, h);

	if (r1 <= r4 && r2 > r4){
		printf("%.3f", 2.0*(r2-r4));
		return 0;
	}

	if (r2 <= r4 && r1 > r4){
		printf("%.3f", 2.0*(r1 - r4));
		return 0;
	}

	if (r1 <= r4 && r2 <= r4){
		printf("%.3f", 0.0);
		return 0;		
	}
	printf("%.3f", ternar1());
}