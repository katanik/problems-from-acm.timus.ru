#include<iostream>
#include<vector>
#include<algorithm>
#include<cmath>
#include<string>
#include<sstream>
#include<set>
#include<iterator>
#include<map>
#include<queue>

using namespace std;
typedef long long ll;
typedef double db;

struct pt {
	db x, y, z;
	pt() {}
	pt(db x1, db y1, db z1) { x = x1, y = y1, z = z1; }
	pt operator^ (pt p) { return pt(y*p.z - z * p.y, z*p.x - x * p.z, x*p.y - y * p.x); }
	db operator* (pt p) { return x * p.x + y * p.y + z * p.z; }
	bool operator== (pt p) { return x == p.x && y == p.y && z == p.z; }
	bool operator!= (pt p) { return !(*this == p); }
	db mod() { return sqrt(x*x + y * y + z * z); }
	bool in() { scanf("%lf%lf%lf", &x, &y, &z); return 1; }
};

int n;
vector <pt> points, used;

int circle(vector <pt> & v, pt p) {
	int k1 = 0, k2 = 0;
	for (int i = 0; i<v.size(); i++) {
		db d = p * v[i];
		if (d>0) k1++;
		if (d<0) k2++;
	}
	return max(k1, k2);
}

int count(pt & a, pt & b) {
	pt norm = a ^ b;
	for (int i = 0; i<used.size(); i++)
		if ((used[i] ^ norm).mod() == 0)
			return 0;
	used.push_back(norm);
	int k1 = 0, k2 = 0, mx = 0;
	vector <pt> v;
	for (int i = 0; i<n; i++) {
		db p = (a^b)*points[i];
		if (p>0) { k1++; continue; }
		if (p<0) { k2++; continue; }
		v.push_back(points[i]);
	}
	for (int i = 0; i<v.size(); i++)
		mx = max(mx, circle(v, ((a^b) ^ v[i])));
	return max(k1, k2) + mx + 1;
}

int main() {
	int mx = 0;
	scanf("%d", &n);
	points.resize(n);
	for (int i = 0; i<n; i++)
		points[i].in();
	for (int i = 0; i<n - 1; i++)
		for (int j = i + 1; j<n; j++)
			if ((points[i] ^ points[j]).mod() != 0)
				mx = max(mx, count(points[i], points[j]));
	printf("%d", max(1, mx));
}
