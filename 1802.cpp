#include<iostream>
#include<vector>
#include<algorithm>
#include<cmath>
#include<string>
#include<sstream>
#include<set>
#include<iterator>
#include<map>
#include<queue>

using namespace std;
typedef long long ll;
typedef double db;
#define v_i vector<int>
#define vv_i vector<vector<int> >
#define vvv_i vector<vector<vector<int> > >
#define vvvv_i vector<vector<vector<vector<int> > > >
#define v_c vector<char>
#define vv_c vector<vector<char> >
#define vvv_c vector<vector<vector<char> > >
#define forn(i,p,n) for (int i=p; i<n; i++)
#define v_b vector <bool>

vvvv_i g (6,vvv_i(4));
vvv_c a, ans;
int n;
bool f=0;
v_b used(6,0);

bool check (int & ii, int & jj, int & side){
	if (side==1) forn (i,0,n) forn(j,0,n) forn(k,0,g[ii][jj][i][j]) if (a[n-i-1][j][n-k-1]!='-') return 0;
	if (side==2) forn (i,0,n) forn(j,0,n) forn(k,0,g[ii][jj][i][j]) if (a[n-k-1][j][i]!='-') return 0;
	if (side==3) forn (i,0,n) forn(j,0,n) forn(k,0,g[ii][jj][i][j]) if (a[k][j][n-i-1]!='-') return 0;
	if (side==4) forn (i,0,n) forn(j,0,n) forn(k,0,g[ii][jj][i][j]) if (a[n-i-1][k][j]!='-') return 0;
	if (side==5) forn (i,0,n) forn(j,0,n) forn(k,0,g[ii][jj][i][j]) if (a[n-i-1][n-k-1][n-j-1]!='-') return 0;

	if (side==1) forn (i,0,n) forn(j,0,n) forn(k,0,g[ii][jj][i][j]) a[n-i-1][j][n-k-1]=char(ii+'1');
	if (side==2) forn (i,0,n) forn(j,0,n) forn(k,0,g[ii][jj][i][j]) a[n-k-1][j][i]=char(ii+'1');
	if (side==3) forn (i,0,n) forn(j,0,n) forn(k,0,g[ii][jj][i][j]) a[k][j][n-i-1]=char(ii+'1');
	if (side==4) forn (i,0,n) forn(j,0,n) forn(k,0,g[ii][jj][i][j]) a[n-i-1][k][j]=char(ii+'1');
	if (side==5) forn (i,0,n) forn(j,0,n) forn(k,0,g[ii][jj][i][j]) a[n-i-1][n-k-1][n-j-1]=char(ii+'1');
	
	return 1;
}

void back1 (int ii, int jj, int side) {
	if (side==1) forn (i,0,n) forn(j,0,n) forn(k,0,g[ii][jj][i][j]) a[n-i-1][j][n-k-1]='-';
	if (side==2) forn (i,0,n) forn(j,0,n) forn(k,0,g[ii][jj][i][j]) a[n-k-1][j][i]='-';
	if (side==3) forn (i,0,n) forn(j,0,n) forn(k,0,g[ii][jj][i][j]) a[k][j][n-i-1]='-';
	if (side==4) forn (i,0,n) forn(j,0,n) forn(k,0,g[ii][jj][i][j]) a[n-i-1][k][j]='-';
	if (side==5) forn (i,0,n) forn(j,0,n) forn(k,0,g[ii][jj][i][j]) a[n-i-1][n-k-1][n-j-1]='-';
}

void out (){
	for (int k=0; k<n; k++)
		for (int i=0; i<n; i++){
			for (int j=0; j<n; j++)
				printf("%c ", ans[i][j][k]);
			printf("\n");
		}
}

void bt (int i, int j, int side);

void change_side (int side) {
	forn (i,0,6) 
		if (!used[i]) 
			forn (j,0,4) bt (i, j, side);
}

void bt (int i, int j, int side) {
	if (f || !check(i, j, side)) return;
	used[i]=1;
	if (side>=5) {f=1; ans=a; return;}
	change_side (side+1);
	used[i]=0;
	back1 (i, j, side);
}

vv_i orient (vv_i & v) {
	vv_i new_v=v;
	forn(i,0,v.size())
		forn(j,0,v.size())
		new_v[i][j]=v[v.size()-1-j][i];
	return new_v;
}

void in (int k) {
	vv_i v(n, v_i (n)); 
	forn(i,0,n)
		forn(j,0,n)
		scanf("%d", &v[i][j]);
	g[k][0]=v; 	
	forn(i,0,3)
		g[k][i+1]=orient(g[k][i]);
}


int main(){
	scanf("%d", &n);
	forn (i,0,6) in(i);		
	a.assign(n,vv_c(n,v_c(n,'-')));
	forn (i,0,n) forn(j,0,n) forn(k,0,g[0][0][i][j]) a[i][j][k]=char('1'); 
	used[0]=1;

	change_side (1);

	if (f) {printf("Yes\n"); out();}
	else printf("No");
}